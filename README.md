a project using shortest path algorithm with the plotted points as a Nodes.
The points plotted to determine the ways and intersection where the shortest path could move.

After determining the shortest path from the inputed Start and End Destination using Drawline System 
it will show the final output which is the lines connecting the Start and Goal