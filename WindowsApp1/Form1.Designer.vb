﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMakeGraph = New System.Windows.Forms.Button()
        Me.btnGetShortestPath = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'btnMakeGraph
        '
        Me.btnMakeGraph.Location = New System.Drawing.Point(313, 55)
        Me.btnMakeGraph.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnMakeGraph.Name = "btnMakeGraph"
        Me.btnMakeGraph.Size = New System.Drawing.Size(145, 44)
        Me.btnMakeGraph.TabIndex = 0
        Me.btnMakeGraph.Text = "Make Graph"
        Me.btnMakeGraph.UseVisualStyleBackColor = True
        '
        'btnGetShortestPath
        '
        Me.btnGetShortestPath.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGetShortestPath.Location = New System.Drawing.Point(313, 132)
        Me.btnGetShortestPath.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnGetShortestPath.Name = "btnGetShortestPath"
        Me.btnGetShortestPath.Size = New System.Drawing.Size(145, 44)
        Me.btnGetShortestPath.TabIndex = 1
        Me.btnGetShortestPath.Text = "Shortest Path A to P"
        Me.btnGetShortestPath.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Entrance", "City Asserors Office", "Communication Server Room", "City TreasuryManagement Office", "City Registry Office", "Cultural Affair Tourism Sports And Development", "Multi Purpose", "Business Permit And Licensing Office", "Trycycle Franchising Office", "City Budget Management Office", "City Planning Development Office", "ITC", "Engineering Services Office", "Office Of The Building Officials", "Canteen", "Maintenance Office", "DILG", "City Human Resource Management Office", "Legal Office", "Accounting And Internal Office", "Councilor1", "Councilor2", "Councilor3", "Councilor4", "Councilor5", "Councilor6", "Councilor7", "Councilor8", "Councilor9", "Councilor10", "Councilor11", "Councilor12", "SWMTF", "City Legal Officer", "Public Affairs Office", "City Admin Office", "Office Of The Executive Assistant", "Mayors Office", "Vice Mayors Office", "Session Hall", "ITC Division Office", "SB Staff", "Cooperative And Livelihood Development Department", "Office Of The City Prosecutors", "City Prossecutors Office", "Comelec Office", "Agricultural Services Department", "Municipal Trial Court", "City Social Services And Youth Development", "Treasury Department Records And Operation Division", "Community E Center", "Calamba Disaster Coordinating Council Office", "City Health Service Office", "LandBank", "City Housing And Settlement Department", "POSO", "Bids And Award Comittee"})
        Me.ComboBox1.Location = New System.Drawing.Point(132, 86)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(141, 21)
        Me.ComboBox1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 86)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Starting Point"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 138)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Destination Point"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"Entrance", "City Asserors Office", "Communication Server Room", "City TreasuryManagement Office", "City Registry Office", "Cultural Affair Tourism Sports And Development", "Multi Purpose", "Business Permit And Licensing Office", "Trycycle Franchising Office", "City Budget Management Office", "City Planning Development Office", "ITC", "Engineering Services Office", "Office Of The Building Officials", "Canteen", "Maintenance Office", "DILG", "City Human Resource Management Office", "Legal Office", "Accounting And Internal Office", "Councilor1", "Councilor2", "Councilor3", "Councilor4", "Councilor5", "Councilor6", "Councilor7", "Councilor8", "Councilor9", "Councilor10", "Councilor11", "Councilor12", "SWMTF", "City Legal Officer", "Public Affairs Office", "City Admin Office", "Office Of The Executive Assistant", "Mayors Office", "Vice Mayors Office", "Session Hall", "ITC Division Office", "SB Staff", "Cooperative And Livelihood Development Department", "Office Of The City Prosecutors", "City Prossecutors Office", "Comelec Office", "Agricultural Services Department", "Municipal Trial Court", "City Social Services And Youth Development", "Treasury Department Records And Operation Division", "Community E Center", "Calamba Disaster Coordinating Council Office", "City Health Service Office", "LandBank", "City Housing And Settlement Department", "POSO", "Bids And Award Comittee"})
        Me.ComboBox2.Location = New System.Drawing.Point(132, 138)
        Me.ComboBox2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(141, 21)
        Me.ComboBox2.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(533, 292)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.btnGetShortestPath)
        Me.Controls.Add(Me.btnMakeGraph)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnMakeGraph As Button
    Friend WithEvents btnGetShortestPath As Button
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBox2 As ComboBox
End Class
