﻿Imports System.Drawing.Drawing2D
Public Class groundfloor


    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
    End Sub

    Private Sub groundfloor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Public Sub PictureBox1_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox1.Paint
        Dim blackpen As New Pen(Color.Black, 3)
        blackpen.StartCap = LineCap.ArrowAnchor    
        Dim point1 As New Point
        Dim point2 As New Point
        Dim l As New Integer
        For i As Integer = 0 To Vertex.shortestpath1.Count - 2
            point1 = New Point(Vertex.shortestpath1(i).X, Vertex.shortestpath1(i).Y)
            point2 = New Point(Vertex.shortestpath1(i + 1).X, Vertex.shortestpath1(i + 1).Y)
            e.Graphics.DrawLine(blackpen, point1, point2)
        Next
    End Sub

    Public Sub PictureBox1_MouseMove(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseMove
        Label1.Text = "X =" & e.X & "; Y = " & e.Y
    End Sub

End Class