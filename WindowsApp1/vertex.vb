﻿Public Class Vertex
    Public Value As String      'payload of the vertex
    Public Index As Integer     'every vertex has an ID number
    Public X As Integer        'grid co-ordinate 
    Public Y As Integer        'grid o-ordinate 
    Public g As Integer         'g value (path distance from start) for use by A*
    Public f As Integer         'f value (g + h) for use by A*
    Public myPointsArray As New List(Of Point)
    Public Shared shortestpath1 As List(Of Point)
    Public Shared shortestpath2 As List(Of Point)
    Public Shared shortestpath3 As List(Of Point)
    Public Shared shortestpath4 As List(Of Point)
    Public Parent As Vertex     'previous vertex for use by A*

    Public Sub New(ByVal Index As Integer, ByVal Value As String, ByVal X As Integer, ByVal Y As Integer)
        Me.Index = Index
        Me.Value = Value
        Me.X = X
        Me.Y = Y
    End Sub

    Public Function h(destination As Vertex) As Integer
        'calculate Manhattan distance from this vertex to the desination vertex
        'Return (destination.X - Me.X) + (destination.Y - Me.Y)        
        Return Math.Sqrt(((destination.X - Me.X) ^ 2 + (destination.Y - Me.Y) ^ 2))
    End Function

    Public Function computeG(start As Vertex) As Integer
        Return Math.Sqrt(((start.X - Me.X) ^ 2 + (start.Y - Me.Y) ^ 2))

    End Function

End Class

Public Class Graph

    Private Const TOTAL_VERTICES As Integer = 1000
    Public vertices() As Vertex                   'array of verex objects
    Private adjacencyMatrix(,) As Integer
    Private iVertex As Integer                    'for scanning vertices array

    Public Sub New()
        ReDim vertices(TOTAL_VERTICES)
        ReDim adjacencyMatrix(TOTAL_VERTICES, TOTAL_VERTICES)

        'initialise the adjacency matrix
        Dim x, y As Integer
        For x = 0 To TOTAL_VERTICES - 1
            For y = 0 To TOTAL_VERTICES - 1
                adjacencyMatrix(x, y) = 0
            Next
        Next
        iVertex = 0
    End Sub

    Public Sub AddVertex(ByVal index As Integer, ByVal value As String, ByVal X As Integer, ByVal Y As Integer)
        vertices(iVertex) = New Vertex(index, value, X, Y)
        iVertex += 1
    End Sub

    Public Sub AddEdge(ByVal StartVertex As Integer, ByVal EndVertex As Integer, Weight As Double)
        adjacencyMatrix(StartVertex, EndVertex) = Weight
        adjacencyMatrix(EndVertex, StartVertex) = Weight   'undirected graph is symetrical
    End Sub

    Public Function AStar(start As Integer, destination As Integer) As List(Of Point)
        Dim openVertices As New List(Of Integer) 'this list is the fringe
        Dim closedVertices As New List(Of Integer)
        Dim unvisitedVertices As New List(Of Integer)
        Dim currentVertex As Vertex

        'all vertices are unvisited to begin with
        For i As Integer = 0 To TOTAL_VERTICES - 1
            unvisitedVertices.Add(i)
        Next

        'make the start vertex current
        unvisitedVertices.Remove(start)
        currentVertex = vertices(start)

        'calculate f value for start vertex (g + h)
        vertices(start).g = 0
        vertices(start).f = vertices(start).g + vertices(start).h(vertices(destination))

        While currentVertex IsNot vertices(destination)

            'for each vertex adjacent to current
            For i As Integer = 0 To TOTAL_VERTICES - 1
                If adjacencyMatrix(currentVertex.Index, i) > 0 Then 'it's a neighbour if it shares an edge 

                    'if it's not in closed list and not in open list
                    If (Not (closedVertices.Contains(i))) And (Not (openVertices.Contains(i))) Then
                        unvisitedVertices.Remove(i)
                        openVertices.Add(i)
                        vertices(i).Parent = currentVertex
                    End If

                    'calculate the f value for all open neighbours
                    Dim g As Integer
                    Dim f As Integer
                    g = currentVertex.g + adjacencyMatrix(currentVertex.Index, i)
                    f = g + vertices(i).h(vertices(destination))

                    'update the f value if it is brand new (existing value is 0) 
                    'or new value is smaller than existing value
                    If (vertices(i).f = 0) Or (f < vertices(i).f) Then
                        vertices(i).f = f
                        vertices(i).g = g
                        vertices(i).Parent = currentVertex
                    End If

                End If
            Next

            closedVertices.Add(currentVertex.Index)

            'remove vertex with lowest f value from open list and make it current
            Dim iSmallestf As Integer = 10000 'large value to start
            Dim iNextCurrent As Integer
            For Each i In openVertices
                If vertices(i).f < iSmallestf Then
                    iSmallestf = vertices(i).f
                    iNextCurrent = i
                End If
            Next
            openVertices.Remove(iNextCurrent)
            currentVertex = vertices(iNextCurrent)

        End While

        'generate the path information

        ''reverse of path 
        'MsgBox(vertices(destination).Parent.Value)
        'MsgBox(vertices(destination).Parent.Parent.Value)
        'MsgBox(vertices(destination).Parent.Parent.Parent.Value)
        'MsgBox(vertices(destination).Parent.Parent.Parent.Parent.Value)

        Dim shortestpath As New List(Of String
            )

        Dim v As Vertex
        v = vertices(destination)
        'Do Until v Is Nothing
        '    shortestpath.Add(v.Value)

        '    v = v.Parent
        'Loop
        'shortestpath.Reverse()
        'shortestpath.Add(currentVertex.g) 'returns length of path as a string   
        'Return shortestpath

        Dim points As New List(Of Point)
        Dim ctr As Integer = 0
        Do Until v Is Nothing
            points.Add(New Point(v.X, v.Y))
            v = v.Parent
        Loop

        'Dim openVertices As New List(Of Integer) 'this list is the fringe
        'Dim closedVertices As New List(Of Integer)
        'Dim unvisitedVertices As New List(Of Integer)
        'Dim currentVertex As Vertex
        openVertices.Clear()
        closedVertices.Clear()
        unvisitedVertices.Clear()

        Return points

    End Function
End Class


'lower ground
'g.AddVertex(0, "A", 303, 312)
'g.AddVertex(1, "B", 269, 292)
'g.AddVertex(2, "C", 324, 314)
'g.AddVertex(3, "D", 370, 290)
'g.AddVertex(4, "E", 325, 299)
'g.AddVertex(5, "F", 361, 282)
'g.AddVertex(6, "G", 439, 242)
'g.AddVertex(7, "H", 447, 223)
'g.AddVertex(8, "I", 446, 178)
'g.AddVertex(9, "J", 467, 182)
'g.AddVertex(10, "K", 463, 159)
'g.AddVertex(11, "L", 421, 178)
'g.AddVertex(12, "M", 447, 136)
'g.AddVertex(13, "N", 445, 118)
'g.AddVertex(14, "O", 407, 105)
'g.AddVertex(15, "P", 399, 86)
'g.AddVertex(16, "Q", 341, 63)
'g.AddVertex(17, "R", 269, 64)
'g.AddVertex(18, "S", 223, 52)
'g.AddVertex(19, "T", 193, 62)
'g.AddVertex(20, "U", 138, 97)
'g.AddVertex(21, "V", 75, 227)
'g.AddVertex(22, "W", 82, 250)
'g.AddVertex(23, "X", 132, 272)
'g.AddVertex(24, "Y", 147, 297)
'g.AddVertex(25, "Z", 174, 303)
'g.AddVertex(26, "AA", 254, 269)
'g.AddVertex(27, "BB", 322, 246)
'g.AddVertex(28, "CC", 366, 204)
'g.AddVertex(29, "DD", 366, 186)
'g.AddVertex(30, "EE", 366, 170)
'g.AddVertex(31, "FF", 316, 120)
'g.AddVertex(32, "GG", 255, 116)
'g.AddVertex(33, "HH", 192, 121)
'g.AddVertex(34, "II", 143, 170)
'g.AddVertex(35, "JJ", 142, 204)
'g.AddVertex(36, "KK", 188, 247)
'g.AddVertex(37, "LL", 325, 275)
'g.AddVertex(38, "MM", 186, 277)
'g.AddVertex(39, "NN", 254, 235)
'g.AddVertex(40, "OO", 326, 186)
'g.AddVertex(41, "PP", 252, 134)
'g.AddVertex(42, "QQ", 179, 186)
'g.AddVertex(43, "RR", 141, 184)
'g.AddVertex(44, "SS", 30, 153)
'g.AddVertex(45, "TT", \484, 152)