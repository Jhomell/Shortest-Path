﻿Imports System.Drawing.Drawing2D
Public Class Form1
    Dim g As New Graph


    Private Sub btnMakeGraph_Click(sender As Object, e As EventArgs) Handles btnMakeGraph.Click
        groundfloor.Show()
        lowerground.Show()
        secondfloor.Show()
        thirdfloor.Show()
        'Ground floor 
    End Sub

    Private Sub btnGetShortestPath_Click(sender As Object, e As EventArgs) Handles btnGetShortestPath.Click

        g.AddVertex(0, "A", 255, 357)
        g.AddVertex(1, "B", 254, 307)
        g.AddVertex(2, "C", 324, 302)
        g.AddVertex(3, "D", 194, 306)
        g.AddVertex(4, "E", 335, 310)
        g.AddVertex(5, "F", 408, 261)
        g.AddVertex(6, "G", 448, 230)
        g.AddVertex(7, "H", 379, 262)
        g.AddVertex(8, "I", 425, 224)
        g.AddVertex(9, "J", 437, 186)
        g.AddVertex(10, "K", 425, 142)
        g.AddVertex(11, "L", 383, 110)
        g.AddVertex(12, "M", 393, 98)
        g.AddVertex(13, "N", 323, 65)
        g.AddVertex(14, "O", 255, 66)
        g.AddVertex(15, "P", 221, 48)
        g.AddVertex(16, "Q", 189, 65)
        g.AddVertex(17, "R", 182, 51)
        g.AddVertex(18, "S", 127, 104)
        g.AddVertex(19, "T", 75, 145)
        g.AddVertex(20, "U", 72, 183)
        g.AddVertex(21, "V", 79, 226)
        g.AddVertex(22, "W", 82, 250)
        g.AddVertex(23, "X", 132, 266)
        g.AddVertex(24, "Y", 147, 297)
        g.AddVertex(25, "Z", 256, 266)
        g.AddVertex(26, "A2", 329, 272)
        g.AddVertex(27, "B2", 183, 275)
        g.AddVertex(28, "C2", 321, 245)
        g.AddVertex(29, "D2", 367, 199)
        g.AddVertex(30, "E2", 366, 183)
        g.AddVertex(31, "F2", 367, 165)
        g.AddVertex(32, "G2", 320, 118)
        g.AddVertex(33, "H2", 255, 110)
        g.AddVertex(34, "I2", 195, 120)
        g.AddVertex(35, "J2", 142, 164)
        g.AddVertex(36, "K2", 141, 184)
        g.AddVertex(37, "L2", 141, 204)
        g.AddVertex(38, "M2", 189, 245)
        g.AddVertex(39, "N2", 254, 235)
        g.AddVertex(40, "O2", 328, 186)
        g.AddVertex(41, "P2", 253, 134)
        g.AddVertex(42, "Q2", 181, 182)
        g.AddVertex(43, "R2", 482, 148)
        g.AddVertex(44, "S2", 27, 149)
        g.AddVertex(45, "T2", 256, 92)
        g.AddVertex(46, "U2", 305, 314)
        g.AddVertex(47, "V2", 268, 299)
        g.AddVertex(48, "W2", 325, 312)
        g.AddVertex(49, "X2", 370, 290)
        g.AddVertex(50, "Y2", 439, 240)
        g.AddVertex(51, "Z2", 323, 301)
        g.AddVertex(52, "A3", 398, 260)
        g.AddVertex(53, "B3", 447, 227)
        g.AddVertex(54, "C3", 446, 183)
        g.AddVertex(55, "D3", 163, 183)
        g.AddVertex(56, "E3", 462, 155)
        g.AddVertex(57, "F3", 431, 180)
        g.AddVertex(58, "G3", 449, 135)
        g.AddVertex(59, "H3", 439, 120)
        g.AddVertex(60, "I3", 405, 105)
        g.AddVertex(61, "J3", 397, 91)
        g.AddVertex(62, "K3", 339, 63)
        g.AddVertex(63, "L3", 269, 59)
        g.AddVertex(64, "M3", 310, 56)
        g.AddVertex(65, "N3", 109, 119)
        g.AddVertex(66, "O3", 98, 109)
        g.AddVertex(67, "P3", 87, 139)
        g.AddVertex(68, "Q3", 73, 158)
        g.AddVertex(69, "R3", 72, 182)
        g.AddVertex(70, "S3", 87, 181)
        g.AddVertex(71, "T3", 86, 228)
        g.AddVertex(72, "U3", 96, 243)
        g.AddVertex(73, "V3", 139, 263)
        g.AddVertex(74, "W3", 204, 292)
        g.AddVertex(75, "X3", 210, 315)
        g.AddVertex(76, "Y3", 233, 313)
        g.AddVertex(77, "Z3", 349, 275)
        g.AddVertex(78, "A4", 183, 273)
        g.AddVertex(79, "B4", 515, 146)
        g.AddVertex(80, "C4", 20, 142)
        g.AddVertex(81, "D4", 269, 86)
        g.AddVertex(82, "E4", 288, 377)
        g.AddVertex(83, "F4", 361, 345)
        g.AddVertex(84, "G4", 386, 338)
        g.AddVertex(85, "H4", 419, 310)
        g.AddVertex(86, "I4", 414, 282)
        g.AddVertex(87, "J4", 475, 248)
        g.AddVertex(88, "K4", 449, 248)
        g.AddVertex(89, "L4", 459, 200)
        g.AddVertex(90, "M4", 447, 204)
        g.AddVertex(91, "N4", 449, 166)
        g.AddVertex(92, "O4", 472, 155)
        g.AddVertex(93, "P4", 449, 127)
        g.AddVertex(94, "Q4", 437, 132)
        g.AddVertex(95, "R4", 351, 77)
        g.AddVertex(96, "S4", 359, 60)
        g.AddVertex(97, "T4", 285, 68)
        g.AddVertex(98, "U4", 253, 56)
        g.AddVertex(99, "V4", 283, 48)
        g.AddVertex(100, "W4", 221, 79)
        g.AddVertex(101, "X4", 186, 107)
        g.AddVertex(102, "Y4", 125, 128)
        g.AddVertex(103, "Z4", 137, 132)
        g.AddVertex(104, "A5", 103, 154)
        g.AddVertex(105, "B5", 98, 165)
        g.AddVertex(106, "C5", 121, 167)
        g.AddVertex(107, "D5", 126, 204)
        g.AddVertex(108, "E5", 113, 202)
        g.AddVertex(109, "F5", 121, 241)
        g.AddVertex(110, "G5", 95, 250)
        g.AddVertex(111, "H5", 100, 267)
        g.AddVertex(112, "I5", 125, 270)
        g.AddVertex(113, "J5", 159, 292)
        g.AddVertex(114, "K5", 127, 295)
        g.AddVertex(115, "L5", 151, 309)
        g.AddVertex(116, "M5", 215, 345)
        g.AddVertex(117, "N5", 218, 312)
        g.AddVertex(118, "O5", 361, 307)
        g.AddVertex(119, "P5", 517, 168)
        g.AddVertex(120, "Q5", 63, 170)
        g.AddVertex(121, "R5", 271, 94)
        g.AddVertex(122, "S5", 279, 34)
        g.AddVertex(123, "T5", 360, 342)
        g.AddVertex(124, "U5", 380, 340)
        g.AddVertex(125, "V5", 388, 337)
        g.AddVertex(126, "W5", 427, 309)
        g.AddVertex(127, "X5", 433, 306)
        g.AddVertex(128, "Y5", 471, 288)
        g.AddVertex(129, "Z5", 481, 270)
        g.AddVertex(130, "A6", 422, 294)
        g.AddVertex(131, "B6", 467, 264)
        g.AddVertex(132, "C6", 483, 256)
        g.AddVertex(133, "D6", 458, 250)
        g.AddVertex(134, "E6", 457, 210)
        g.AddVertex(135, "F6", 474, 217)
        g.AddVertex(136, "G6", 471, 206)
        g.AddVertex(137, "H6", 456, 171)
        g.AddVertex(138, "I6", 483, 166)
        g.AddVertex(139, "J6", 440, 135)
        g.AddVertex(140, "K6", 479, 150)
        g.AddVertex(141, "L6", 387, 108)
        g.AddVertex(142, "M6", 363, 82)
        g.AddVertex(143, "N6", 343, 90)
        g.AddVertex(144, "O6", 279, 96)
        g.AddVertex(145, "P6", 206, 90)
        g.AddVertex(146, "Q6", 175, 86)
        g.AddVertex(147, "R6", 175, 97)
        g.AddVertex(148, "S6", 148, 117)
        g.AddVertex(149, "T6", 147, 105)
        g.AddVertex(150, "U6", 127, 116)
        g.AddVertex(151, "V6", 91, 143)
        g.AddVertex(152, "W6", 81, 149)
        g.AddVertex(153, "X6", 93, 156)
        g.AddVertex(154, "Y6", 81, 164)
        g.AddVertex(155, "Z6", 79, 194)
        g.AddVertex(156, "A7", 102, 175)
        g.AddVertex(157, "B7", 103, 210)
        g.AddVertex(158, "C7", 89, 206)
        g.AddVertex(159, "D7", 87, 216)
        g.AddVertex(160, "E7", 100, 245)
        g.AddVertex(161, "F7", 75, 255)
        g.AddVertex(162, "G7", 79, 271)
        g.AddVertex(163, "H7", 86, 278)
        g.AddVertex(164, "I7", 123, 304)
        g.AddVertex(165, "J7", 132, 311)
        g.AddVertex(166, "K7", 167, 338)
        g.AddVertex(167, "L7", 181, 346)
        g.AddVertex(168, "M7", 95, 268)
        g.AddVertex(169, "N7", 139, 300)
        g.AddVertex(170, "O7", 193, 336)
        g.AddVertex(171, "P7", 206, 344)
        g.AddVertex(172, "Q7", 209, 310)
        g.AddVertex(173, "R7", 346, 308)
        g.AddVertex(174, "S7", 516, 178)
        g.AddVertex(175, "T7", 43, 170)
        g.AddVertex(176, "U7", 261, 103)

        g.AddVertex(177, "V7", 397, 98)


        'ground floor
        g.AddEdge(0, 1, 5)

        g.AddEdge(1, 2, 5)
        g.AddEdge(1, 3, 5)
        g.AddEdge(1, 26, 5)
        g.AddEdge(1, 0, 5)
        g.AddEdge(1, 25, 5)
        g.AddEdge(1, 27, 5)


        g.AddEdge(2, 1, 5)
        g.AddEdge(2, 7, 5)
        g.AddEdge(2, 4, 5)


        g.AddEdge(3, 1, 5)
        g.AddEdge(3, 23, 5)
        g.AddEdge(3, 24, 5)

        g.AddEdge(4, 2, 5)

        g.AddEdge(5, 7, 5)


        g.AddEdge(6, 8, 5)

        g.AddEdge(7, 5, 5)
        g.AddEdge(7, 2, 5)
        g.AddEdge(7, 8, 5)

        g.AddEdge(8, 6, 5)
        g.AddEdge(8, 7, 5)
        g.AddEdge(8, 9, 5)

        g.AddEdge(9, 8, 5)
        g.AddEdge(9, 10, 5)

        g.AddEdge(10, 43, 5)
        g.AddEdge(10, 9, 5)
        g.AddEdge(10, 11, 5)

        g.AddEdge(11, 12, 5)
        g.AddEdge(11, 10, 5)
        g.AddEdge(11, 13, 5)

        g.AddEdge(12, 11, 5)


        g.AddEdge(13, 11, 5)
        g.AddEdge(13, 14, 5)

        g.AddEdge(14, 13, 5)
        g.AddEdge(14, 15, 5)
        g.AddEdge(14, 45, 5)
        g.AddEdge(14, 16, 5)

        g.AddEdge(15, 14, 5)

        g.AddEdge(16, 14, 5)
        g.AddEdge(16, 17, 5)
        g.AddEdge(16, 18, 5)



        g.AddEdge(17, 16, 5)

        g.AddEdge(18, 16, 5)
        g.AddEdge(18, 19, 5)

        g.AddEdge(19, 18, 5)
        g.AddEdge(19, 20, 5)
        g.AddEdge(19, 44, 5)

        g.AddEdge(20, 19, 5)
        g.AddEdge(20, 21, 5)

        g.AddEdge(21, 22, 5)
        g.AddEdge(21, 20, 5)
        g.AddEdge(21, 23, 5)

        g.AddEdge(22, 21, 5)

        g.AddEdge(23, 21, 5)
        g.AddEdge(23, 3, 5)
        g.AddEdge(23, 24, 5)

        g.AddEdge(24, 23, 5)
        g.AddEdge(24, 3, 5)


        g.AddEdge(25, 1, 5)
        g.AddEdge(25, 39, 5)
        g.AddEdge(25, 28, 5)


        g.AddEdge(26, 1, 5)
        g.AddEdge(26, 77, 5)


        g.AddEdge(27, 1, 5)
        g.AddEdge(27, 78, 5)

        g.AddEdge(28, 25, 5)
        g.AddEdge(28, 29, 5)

        g.AddEdge(29, 30, 5)
        g.AddEdge(29, 28, 5)


        g.AddEdge(30, 29, 5)
        g.AddEdge(30, 31, 5)
        g.AddEdge(30, 40, 5)

        g.AddEdge(31, 30, 5)
        g.AddEdge(31, 32, 5)

        g.AddEdge(32, 31, 5)
        g.AddEdge(32, 33, 5)

        g.AddEdge(33, 32, 5)
        g.AddEdge(33, 34, 5)
        g.AddEdge(33, 41, 5)

        g.AddEdge(34, 33, 5)
        g.AddEdge(34, 35, 5)

        g.AddEdge(35, 34, 5)
        g.AddEdge(35, 36, 5)

        g.AddEdge(36, 42, 5)
        g.AddEdge(36, 35, 5)
        g.AddEdge(36, 37, 5)

        g.AddEdge(37, 36, 5)
        g.AddEdge(37, 38, 5)

        g.AddEdge(38, 37, 5)

        g.AddEdge(39, 25, 5)

        g.AddEdge(40, 30, 5)

        g.AddEdge(41, 33, 5)

        g.AddEdge(42, 36, 5)

        g.AddEdge(43, 10, 5)
        g.AddEdge(43, 79, 5)
        g.AddEdge(43, 119, 5)


        g.AddEdge(44, 19, 5)
        g.AddEdge(44, 80, 5)
        g.AddEdge(44, 120, 5)

        g.AddEdge(45, 81, 5)
        g.AddEdge(45, 14, 5)
        g.AddEdge(45, 121, 5)

        g.AddEdge(46, 51, 5)
        g.AddEdge(46, 47, 5)

        g.AddEdge(47, 46, 5)
        g.AddEdge(47, 76, 5)

        g.AddEdge(48, 51, 5)

        g.AddEdge(49, 51, 5)

        g.AddEdge(50, 52, 5)

        g.AddEdge(51, 46, 5)
        g.AddEdge(51, 48, 5)
        g.AddEdge(51, 49, 5)
        g.AddEdge(51, 77, 5)

        g.AddEdge(52, 50, 5)
        g.AddEdge(52, 51, 5)
        g.AddEdge(52, 53, 5)

        g.AddEdge(53, 52, 5)
        g.AddEdge(53, 54, 5)

        g.AddEdge(54, 56, 5)
        g.AddEdge(54, 55, 5)
        g.AddEdge(54, 57, 5)
        g.AddEdge(54, 58, 5)

        g.AddEdge(55, 54, 5)

        g.AddEdge(56, 54, 5)

        g.AddEdge(57, 54, 5)

        g.AddEdge(58, 79, 5)
        g.AddEdge(58, 59, 5)
        g.AddEdge(58, 54, 5)
        g.AddEdge(58, 60, 5)

        g.AddEdge(59, 58, 5)

        g.AddEdge(60, 61, 5)
        g.AddEdge(60, 58, 5)
        g.AddEdge(60, 62, 5)

        g.AddEdge(61, 60, 5)

        g.AddEdge(62, 60, 5)
        g.AddEdge(62, 64, 5)
        g.AddEdge(62, 63, 5)

        g.AddEdge(63, 62, 5)
        g.AddEdge(63, 64, 5)
        g.AddEdge(63, 81, 5)

        g.AddEdge(64, 63, 5)
        g.AddEdge(64, 65, 5)

        g.AddEdge(65, 66, 5)
        g.AddEdge(65, 64, 5)
        g.AddEdge(65, 67, 5)

        g.AddEdge(66, 65, 5)

        g.AddEdge(67, 68, 5)
        g.AddEdge(67, 80, 5)
        g.AddEdge(67, 68, 5)

        g.AddEdge(68, 67, 5)

        g.AddEdge(69, 70, 5)

        g.AddEdge(70, 67, 5)
        g.AddEdge(70, 69, 5)
        g.AddEdge(70, 71, 5)

        g.AddEdge(71, 70, 5)
        g.AddEdge(71, 72, 5)
        g.AddEdge(71, 73, 5)

        g.AddEdge(72, 71, 5)

        g.AddEdge(73, 74, 5)
        g.AddEdge(73, 71, 5)

        g.AddEdge(74, 73, 5)
        g.AddEdge(74, 75, 5)
        g.AddEdge(74, 76, 5)
        g.AddEdge(74, 78, 5)

        g.AddEdge(75, 74, 5)

        g.AddEdge(76, 74, 5)
        g.AddEdge(76, 47, 5)

        g.AddEdge(77, 51, 5)

        g.AddEdge(78, 74, 5)

        g.AddEdge(79, 58, 5)
        g.AddEdge(79, 43, 5)

        g.AddEdge(80, 67, 5)
        g.AddEdge(80, 44, 5)

        g.AddEdge(81, 63, 5)
        g.AddEdge(81, 45, 5)

        g.AddEdge(82, 118, 5)
        g.AddEdge(82, 117, 5)
        g.AddEdge(82, 83, 5)
        g.AddEdge(82, 116, 5)

        g.AddEdge(83, 82, 5)
        g.AddEdge(83, 84, 5)
        g.AddEdge(83, 86, 5)

        g.AddEdge(84, 83, 5)

        g.AddEdge(85, 86, 5)

        g.AddEdge(86, 85, 5)
        g.AddEdge(86, 83, 5)
        g.AddEdge(86, 87, 5)

        g.AddEdge(87, 86, 5)
        g.AddEdge(87, 88, 5)

        g.AddEdge(88, 87, 5)
        g.AddEdge(88, 90, 5)

        g.AddEdge(89, 90, 5)

        g.AddEdge(90, 89, 5)
        g.AddEdge(90, 88, 5)
        g.AddEdge(90, 91, 5)

        g.AddEdge(91, 90, 5)
        g.AddEdge(91, 119, 5)
        g.AddEdge(91, 92, 5)

        g.AddEdge(92, 91, 5)
        g.AddEdge(92, 94, 5)

        g.AddEdge(93, 94, 5)

        g.AddEdge(94, 93, 5)
        g.AddEdge(94, 95, 5)
        g.AddEdge(94, 92, 5)

        g.AddEdge(95, 96, 5)
        g.AddEdge(95, 94, 5)
        g.AddEdge(95, 97, 5)

        g.AddEdge(96, 95, 5)

        g.AddEdge(97, 98, 5)
        g.AddEdge(97, 99, 5)
        g.AddEdge(97, 95, 5)
        g.AddEdge(97, 100, 5)
        g.AddEdge(97, 121, 5)

        g.AddEdge(98, 97, 5)

        g.AddEdge(99, 97, 5)

        g.AddEdge(100, 97, 5)
        g.AddEdge(100, 101, 5)

        g.AddEdge(101, 100, 5)
        g.AddEdge(101, 103, 5)

        g.AddEdge(102, 103, 5)

        g.AddEdge(103, 101, 5)
        g.AddEdge(103, 102, 5)
        g.AddEdge(103, 104, 5)

        g.AddEdge(104, 103, 5)
        g.AddEdge(104, 105, 5)

        g.AddEdge(105, 106, 5)
        g.AddEdge(105, 120, 5)

        g.AddEdge(106, 105, 5)
        g.AddEdge(106, 107, 5)

        g.AddEdge(107, 106, 5)
        g.AddEdge(107, 108, 5)
        g.AddEdge(107, 109, 5)

        g.AddEdge(108, 107, 5)

        g.AddEdge(109, 107, 5)
        g.AddEdge(109, 110, 5)

        g.AddEdge(110, 109, 5)
        g.AddEdge(110, 111, 5)
        g.AddEdge(110, 112, 5)

        g.AddEdge(111, 110, 5)

        g.AddEdge(112, 110, 5)
        g.AddEdge(112, 114, 5)
        g.AddEdge(112, 113, 5)

        g.AddEdge(113, 112, 5)
        g.AddEdge(113, 115, 5)
        g.AddEdge(113, 114, 5)
        g.AddEdge(113, 116, 5)


        g.AddEdge(114, 112, 5)
        g.AddEdge(114, 113, 5)

        g.AddEdge(115, 113, 5)

        g.AddEdge(116, 113, 5)
        g.AddEdge(116, 82, 5)

        g.AddEdge(117, 82, 5)

        g.AddEdge(118, 82, 5)

        g.AddEdge(119, 91, 5)
        g.AddEdge(119, 43, 5)
        g.AddEdge(119, 174, 5)

        g.AddEdge(120, 105, 5)
        g.AddEdge(120, 44, 5)
        g.AddEdge(12, 175, 5)

        g.AddEdge(121, 97, 5)
        g.AddEdge(121, 45, 5)

        g.AddEdge(122, 172, 5)
        g.AddEdge(122, 173, 5)
        g.AddEdge(122, 123, 5)
        g.AddEdge(122, 171, 5)

        g.AddEdge(123, 122, 5)
        g.AddEdge(123, 124, 5)
        g.AddEdge(123, 125, 5)
        g.AddEdge(123, 130, 5)

        g.AddEdge(124, 123, 5)

        g.AddEdge(125, 123, 5)

        g.AddEdge(126, 130, 5)

        g.AddEdge(127, 130, 5)

        g.AddEdge(128, 131, 5)

        g.AddEdge(129, 131, 5)

        g.AddEdge(130, 126, 5)
        g.AddEdge(130, 127, 5)
        g.AddEdge(130, 123, 5)

        g.AddEdge(131, 128, 5)
        g.AddEdge(131, 129, 5)
        g.AddEdge(131, 130, 5)
        g.AddEdge(131, 132, 5)

        g.AddEdge(132, 131, 5)
        g.AddEdge(132, 133, 5)

        g.AddEdge(133, 132, 5)
        g.AddEdge(133, 134, 5)

        g.AddEdge(134, 135, 5)
        g.AddEdge(134, 136, 5)
        g.AddEdge(134, 133, 5)
        g.AddEdge(134, 137, 5)

        g.AddEdge(135, 134, 5)

        g.AddEdge(136, 134, 5)

        g.AddEdge(137, 134, 5)
        g.AddEdge(137, 174, 5)
        g.AddEdge(137, 138, 5)

        g.AddEdge(138, 137, 5)
        g.AddEdge(138, 140, 5)
        g.AddEdge(138, 139, 5)

        g.AddEdge(139, 138, 5)
        g.AddEdge(139, 141, 5)

        g.AddEdge(140, 138, 5)

        g.AddEdge(141, 139, 5)
        g.AddEdge(141, 142, 5)
        g.AddEdge(141, 177, 5)

        g.AddEdge(142, 141, 5)
        g.AddEdge(142, 143, 5)

        g.AddEdge(143, 142, 5)
        g.AddEdge(143, 144, 5)

        g.AddEdge(144, 143, 5)
        g.AddEdge(144, 145, 5)
        g.AddEdge(144, 176, 5)

        g.AddEdge(145, 144, 5)
        g.AddEdge(145, 147, 5)

        g.AddEdge(146, 147, 5)

        g.AddEdge(147, 145, 5)
        g.AddEdge(147, 146, 5)
        g.AddEdge(147, 148, 5)

        g.AddEdge(148, 149, 5)
        g.AddEdge(148, 150, 5)
        g.AddEdge(148, 147, 5)
        g.AddEdge(148, 153, 5)

        g.AddEdge(149, 148, 5)

        g.AddEdge(150, 148, 5)

        g.AddEdge(151, 153, 5)

        g.AddEdge(152, 123, 5)

        g.AddEdge(153, 148, 5)
        g.AddEdge(153, 151, 5)
        g.AddEdge(153, 152, 5)
        g.AddEdge(153, 154, 5)

        g.AddEdge(154, 153, 5)
        g.AddEdge(154, 155, 5)

        g.AddEdge(155, 154, 5)
        g.AddEdge(155, 156, 5)
        g.AddEdge(155, 175, 5)

        g.AddEdge(156, 155, 5)
        g.AddEdge(156, 157, 5)

        g.AddEdge(157, 156, 5)
        g.AddEdge(157, 158, 5)
        g.AddEdge(157, 159, 5)

        g.AddEdge(158, 157, 5)

        g.AddEdge(159, 157, 5)

        g.AddEdge(160, 157, 5)
        g.AddEdge(160, 161, 5)

        g.AddEdge(161, 160, 5)
        g.AddEdge(161, 168, 5)

        g.AddEdge(162, 168, 5)

        g.AddEdge(163, 168, 5)

        g.AddEdge(164, 169, 5)

        g.AddEdge(165, 169, 5)

        g.AddEdge(166, 170, 5)

        g.AddEdge(167, 170, 5)

        g.AddEdge(168, 161, 5)
        g.AddEdge(168, 162, 5)
        g.AddEdge(168, 163, 5)
        g.AddEdge(168, 169, 5)

        g.AddEdge(169, 168, 5)
        g.AddEdge(169, 164, 5)
        g.AddEdge(169, 165, 5)
        g.AddEdge(169, 170, 5)

        g.AddEdge(170, 169, 5)
        g.AddEdge(170, 171, 5)
        g.AddEdge(170, 166, 5)
        g.AddEdge(170, 167, 5)

        g.AddEdge(171, 170, 5)
        g.AddEdge(171, 122, 5)

        g.AddEdge(172, 122, 5)

        g.AddEdge(173, 122, 5)

        g.AddEdge(174, 137, 5)
        g.AddEdge(174, 119, 5)

        g.AddEdge(175, 155, 5)
        g.AddEdge(175, 120, 5)

        g.AddEdge(176, 144, 5)

        g.AddEdge(177, 141, 5)
        'MsgBox("Graph Created")

        'isang floor lang
        If ComboBox1.SelectedItem = "Entrance" And ComboBox2.SelectedItem = "City Asserors Office" Then
            Vertex.shortestpath1 = g.AStar(0, 12)
            For i As Integer = 0 To Vertex.shortestpath1.Count - 1
                MsgBox(Vertex.shortestpath1(i).ToString)
            Next
            groundfloor.Show()
        End If

        '1st to second
        If ComboBox1.SelectedItem = "Entrance" And ComboBox2.SelectedItem = "DILG" Then
            'una 0 - hagdan ng 1st floor papuntang second floor
            Vertex.shortestpath1 = g.AStar(0, 44)
            'For i As Integer = 0 To Vertex.shortestpath.Count - 1
            '    MsgBox(Vertex.shortestpath(i).ToString)
            'Next
            groundfloor.Show()
            Vertex.shortestpath2 = g.AStar(120, 102)
            secondfloor.Show()


        End If
    End Sub



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class





